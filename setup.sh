#!/bin/sh

source .venv/bin/activate
pip install -r requirements.txt
ansible-galaxy install -r requirements.yml